package kr.wisenut.ichat_script;

import kr.wisenut.ichat_script.data_object.parser.TypedKeyValue;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 대화 작업 중 저장 해야 할 변수를 Key - value 형태로 Map에 저장
 *                          (변수 이름 , 변수 값)
 */
public class DialogMemory {

    private Map<String, TypedKeyValue> variableMap = new HashMap<>();

    public void setMemoryValue(String variableName, TypedKeyValue value) {
        variableMap.put(variableName, value);
    }

    public TypedKeyValue getMemoryValue(String variableName) {
        return variableMap.get(variableName);
    }

    public List<String> getKeyList() {
        List<String> list = new ArrayList<>(variableMap.keySet());
        return list;
    }

    @Override
    public String toString() {
        return "DialogMemory{" +
                "variableMap=" + variableMap +
                '}';
    }
}
