package kr.wisenut.ichat_script;

import kr.wisenut.ichat_script.data_object.DialogScriptData.DialogScript;
import kr.wisenut.ichat_script.data_object.DialogScriptData.DialogScriptRow;
import kr.wisenut.ichat_script.data_object.arguments.*;
import kr.wisenut.ichat_script.data_object.parser.*;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 대화 작업 실행
 * main 함수에 csv파일 이름 입력 시 해당 대화 작업 시행
 */
public class ScriptDialogTask {
    private List<DialogScriptRow> dialogScriptRowList;
    private int currentRowIndex = 0;
    private DialogMemory dialogMemory;
    private Map<String, Integer> label2IndexMap;

    public ScriptDialogTask(DialogMemory dialogMemory, DialogScript dialogScript) {
        this.dialogMemory = dialogMemory;
        dialogScriptRowList = dialogScript.getRowList();
        initializeLabels();
    }

    /**
     * 대화 작업 초기화
     */
    private void initializeLabels() {
        label2IndexMap = new HashMap<>();
        for (int i = 0; i < dialogScriptRowList.size(); i++) {
            String labelName = dialogScriptRowList.get(i).getLabel();
            if (labelName == null || labelName.isEmpty()) {
                continue;
            }
            label2IndexMap.put(labelName, i);
        }
    }

    /**
     * 다음 대화 작업이 있는지 체크
     *
     * @return true/false
     */
    public boolean hasNext() {
        if (currentRowIndex >= dialogScriptRowList.size()) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * 다음 대화 작업 부르기
     *
     * @throws Exception
     */
    public void executeNext() throws Exception {
        executeRow(currentRowIndex);
    }

    /**
     * 사용자로 부터 입력 받기
     *
     * @return
     * @throws IOException
     */
    public String getMessageFromUser() throws IOException {
        BufferedReader bf = new BufferedReader(new InputStreamReader(System.in));
        String userInput = " ";
        while (!userInput.equalsIgnoreCase("exit")) {
            userInput = bf.readLine();
            return userInput;
        }
        return null;
    }

    /**
     * Index 증가
     */
    private void increaseNextRowIndex() {
        this.currentRowIndex++;
    }

    /**
     * 해당 Label 로 대화 이동
     *
     * @param label
     * @throws Exception
     */
    private void setNextRowIndexByLabel(String label) throws Exception {
        Integer index = label2IndexMap.get(label);
        if (index == null) {
            throw new Exception("NO Such label");
        }
        // TODO index예외처리
        this.currentRowIndex = index;
    }

    /**
     * 사용자에게 메세지 전송
     *
     * @param message
     * @throws IOException
     */
    private void sendMessageToUser(String message) throws IOException {
        System.out.println(message);
    }

    /**
     * 조건 표현식 판단
     * 조건 표현식 String을 인자로 받아 참,거짓 여부 판단
     *
     * @param conditionStr
     * @return true/false
     * @throws ConditionParseException ,IOException, EntityValueTypeMismatchException
     */
    private boolean checkCondition(String conditionStr) throws ConditionParseException, IOException, EntityValueTypeMismatchException {
        //입력된 조건식 문장
        String condition = conditionStr;
        //파서를 통해 논리곱 표준형 형태로 변경
        TempConditionParser parser = new TempConditionParser();
        List<ConditionClause> clauseList = parser.parse(condition);
        //사용자가 입력한 입력값
        Map<String, TypedKeyValue> inputMap = new HashMap<>();
        List<String> list = dialogMemory.getKeyList();
        for (String key : list) {
            inputMap.put(key, new TypedKeyValue(key, dialogMemory.getMemoryValue(key).getValueType(), dialogMemory.getMemoryValue(key).getValue()));
        }
        //조건 처리 결과
        ConditionHandler handler = new ConditionHandler();
        boolean result = handler.isMatchedCondition(clauseList, inputMap);
        return result;
    }

    /**
     * dialogScriptRow의 명령어 (cmd)에 따라서 각각의 함수 매칭
     *
     * @param rowIndex
     * @throws Exception
     */
    private void executeRow(int rowIndex) throws Exception {
        DialogScriptRow dialogScriptRow = dialogScriptRowList.get(rowIndex);
        switch (dialogScriptRow.getCmd()) {
            case sendMessage:
                sendMessage((ArgumentsSendMessage) dialogScriptRow.getArguments());
                break;
            case askAndListen:
                askAndListen((ArgumentsAskAndListen) dialogScriptRow.getArguments());
                break;
            case JumpToLabel_if:
                JumpToLabelIf((ArgumentsJumpToLabel_if) dialogScriptRow.getArguments());
                break;
            case JumpToLabel:
                JumpToLabel((ArgumentsJumpToLabel) dialogScriptRow.getArguments());
                break;
            case JumpToLabel_switch:
                JumpToLabelSwitch((ArgumentsJumpToLabel_switch) dialogScriptRow.getArguments());
                break;
        }

    }


    private void sendMessage(ArgumentsSendMessage argumentsSendMessage) throws IOException {
        sendMessageToUser(argumentsSendMessage.getMessageToUser());
        increaseNextRowIndex();
    }

    private void askAndListen(ArgumentsAskAndListen argumentsAskAndListen) throws IOException {
        sendMessageToUser(argumentsAskAndListen.getPromptMessage());
        String receiveMessage = getMessageFromUser();
        String dataType = argumentsAskAndListen.getDataType();
        TypedKeyValue typedKeyValue = null;
        try {
            switch (dataType) {
                case "Integer":
                    typedKeyValue = new TypedKeyValue(receiveMessage, Integer.parseInt(receiveMessage));
                    break;
                case "Double":
                    typedKeyValue = new TypedKeyValue(receiveMessage, Double.parseDouble(receiveMessage));
                    break;
                case "String":
                    typedKeyValue = new TypedKeyValue(receiveMessage, receiveMessage);
                    break;
                default:
                    throw new IllegalStateException("Unexpected value: " + dataType);
            }
            String variableName = argumentsAskAndListen.getVariable();
            dialogMemory.setMemoryValue(variableName, typedKeyValue);
            increaseNextRowIndex();

        } catch (NumberFormatException e) {
            System.out.println("숫자 형식 으로 입력해 주세요");
            askAndListen(argumentsAskAndListen);
        } catch (Exception e) {

        }

    }

    private void JumpToLabelIf(ArgumentsJumpToLabel_if argumentsJumpToLabel_if) throws Exception {
        String conditionalStr = argumentsJumpToLabel_if.getConditional();
        String jumpLabelIfTrue = argumentsJumpToLabel_if.getTrueLabel();
        String jumpLabelIfFalse = argumentsJumpToLabel_if.getFalseLabel();
        if (checkCondition(conditionalStr)) {
            setNextRowIndexByLabel(jumpLabelIfTrue);
        } else {
            setNextRowIndexByLabel(jumpLabelIfFalse);
        }

    }


    private void JumpToLabel(ArgumentsJumpToLabel argumentsJumpToLabel) throws Exception {
        String nextLabel = argumentsJumpToLabel.getLabel();
        setNextRowIndexByLabel(nextLabel);

    }

    private void JumpToLabelSwitch(ArgumentsJumpToLabel_switch argumentsJumpToLabel_switch) throws Exception {
        String variable = argumentsJumpToLabel_switch.getVariable();
        List<SwitchCaseValue> labelList = argumentsJumpToLabel_switch.getLabelList();
        String userInputValue = (dialogMemory.getMemoryValue(variable)).getValue();
        boolean checkDefault = true;
        for (SwitchCaseValue s : labelList) {
            String nextLabel = s.getLabel();
            String value = s.getValue();
            if (userInputValue.equals(value)) {
                setNextRowIndexByLabel(nextLabel);
                checkDefault = false;
                break;
            }
        }
        if (checkDefault) setNextRowIndexByLabel("default");
    }


    public static void main(String[] args) throws Exception {
        DialogScriptParser parser = new DialogScriptParserImpl();
        DialogMemory dialogMemory = new DialogMemory();
        // parse를 이용해 csv 파일에 저장된 것 파싱
        ScriptDialogTask scriptDialogTask = new ScriptDialogTask(dialogMemory, parser.loadCSV("/newTestCmdData4.csv"));
        //대화가 끝일 때가지 수행
        while (!scriptDialogTask.hasNext()) {
            scriptDialogTask.executeNext();
        }
    }
}
