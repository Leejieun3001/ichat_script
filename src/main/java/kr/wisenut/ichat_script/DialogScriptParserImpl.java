package kr.wisenut.ichat_script;

import com.opencsv.CSVReader;
import kr.wisenut.ichat_script.data_object.DialogScriptData.DialogScript;
import kr.wisenut.ichat_script.data_object.DialogScriptData.DialogScriptRow;
import kr.wisenut.ichat_script.data_object.arguments.*;
import org.apache.commons.io.FileUtils;

import java.io.*;
import java.util.ArrayList;
import java.util.List;

import static kr.wisenut.ichat_script.data_object.DialogScriptData.DialogScriptCmdType.*;
import static kr.wisenut.ichat_script.data_object.DialogScriptData.DialogScriptCmdType.JumpToLabel_switch;

public class DialogScriptParserImpl implements DialogScriptParser {

    private static String directory = "cmdData";
    private static String fileName = "newCmdData";
    private static DialogScriptRow row = new DialogScriptRow();
    private static List<DialogScriptRow> rowList = new ArrayList<>();


    public DialogScriptParserImpl() throws IOException {
        FileUtils.forceMkdir(new File(directory));
    }

    /**
     * 대화가 저장된 csv 파일 읽어서 저장
     *
     * @param csvFileName : 읽어올 CSV 파일 이름
     * @return dialogScript : 저장된 DialogScript 반환
     * @throws IOException
     */
    @Override
    public DialogScript loadCSV(String csvFileName) throws IOException {
        fileName = csvFileName;
        CSVReader csvReader = new CSVReader(new InputStreamReader(new FileInputStream(directory + "/" + fileName), "utf-8"));
        String[] cmdRow;
        String label = null;
        String cmd = null;

        while ((cmdRow = csvReader.readNext()) != null) {
            if (cmdRow.length < 3) continue; // 잘못된 데이터
            cmd = cmdRow[1];
            label = cmdRow[0];

            //명령어에 따라 각각 맞는 형태로 저장
            if (cmd.equals("sendMessage")) {
                ArgumentsSendMessage argumentsSendMessage = new ArgumentsSendMessage(cmdRow[2]);
                row = new DialogScriptRow(label, sendMessage, argumentsSendMessage);

            } else if (cmd.equals("receiveMessage")) {
                ArgumentsReceiveMessage argumentsReceiveMessage = new ArgumentsReceiveMessage(cmdRow[2]);
                row = new DialogScriptRow(label, receiveMessage, argumentsReceiveMessage);

            } else if (cmd.equals("askAndListen")) {
                ArgumentsAskAndListen argumentAskAndListen = new ArgumentsAskAndListen(cmdRow[2], cmdRow[3], cmdRow[4]);
                row = new DialogScriptRow(label, askAndListen, argumentAskAndListen);

            } else if (cmd.equals("JumpToLabel_if") && cmdRow.length == 5) {
                ArgumentsJumpToLabel_if argumentJumpToLabel_if = new ArgumentsJumpToLabel_if(cmdRow[2], cmdRow[3], cmdRow[4]);
                row = new DialogScriptRow(label, JumpToLabel_if, argumentJumpToLabel_if);

            } else if (cmd.equals("JumpToLabel")) {
                ArgumentsJumpToLabel argumentJumpToLabel = new ArgumentsJumpToLabel(cmdRow[2]);
                row = new DialogScriptRow(label, JumpToLabel, argumentJumpToLabel);

            } else if (cmd.equals("JumpToLabel_switch") && cmdRow.length > 5) {
                String variable = cmdRow[2];
                int i = 3;
                List<SwitchCaseValue> switchCaseList = new ArrayList<>();
                while (true) {
                    String value = cmdRow[i];
                    String nextLabel = cmdRow[i + 1];
                    SwitchCaseValue switchCaseValue = new SwitchCaseValue(value, nextLabel);
                    switchCaseList.add(switchCaseValue);
                    if (nextLabel.equals("default")) break;
                    else i += 2;
                }
                ArgumentsJumpToLabel_switch argumentJumpToLabel_switch = new ArgumentsJumpToLabel_switch(variable, switchCaseList);
                row = new DialogScriptRow(label, JumpToLabel_switch, argumentJumpToLabel_switch);
            }
            rowList.add(row);
        }
        DialogScript dialogScript = new DialogScript(rowList);
        return dialogScript;
    }

    public static void main(String[] args) throws IOException {
        DialogScriptParser parser = new DialogScriptParserImpl();
        DialogScript dialogScript = parser.loadCSV("/newTestCmdData1.csv");
        DialogScriptUtil.printData(dialogScript.getRowList());
    }
}
