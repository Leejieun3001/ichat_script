package kr.wisenut.ichat_script;

import kr.wisenut.ichat_script.data_object.commons.StringUtils;
import kr.wisenut.ichat_script.data_object.parser.*;

import java.util.ArrayList;
import java.util.List;

/**
 * 조건식 판별
 * wise i chat V3의  '신규조건분기형 대화작업 개발' 에서 필요한 부분만 가져옴
 * 조건식을 입력 하면 true/false 판별
 */

public class TempConditionParser {
    public List<ConditionClause> parse(String conditionStr) throws ConditionParseException {
        if (StringUtils.isEmpty(conditionStr)) {
            throw new ConditionParseException();
        }

        List<ConditionClause> clauseList = new ArrayList<>();
        String[] clauses = conditionStr.split("and");
        for(String clause : clauses){
            List<ConditionLiteral> literalObjList = new ArrayList<>();
            String[] literals = clause.split("or");
            for(String literal : literals){
                ConditionLiteral literalObj = parseLiteral(literal);
                literalObjList.add(literalObj);
            }
            clauseList.add(new ConditionClause(literalObjList));
        }

        return clauseList;
    }

    private ConditionLiteral parseLiteral(String literalStr) throws ConditionParseException {
        if (literalStr.contains(LiteralOperator.Undefined.getValue())) {
            throw new ConditionParseException(literalStr);
        }

        for (String operator : LiteralOperator.strValues()) {
            if (literalStr.contains(operator)) {
                String[] pair = literalStr.split(operator, 2);
                if(pair.length!=2){
                    throw new ConditionParseException(literalStr);
                }
                return new ConditionLiteral(pair[0].trim(), LiteralOperator.fromText(operator), pair[1].trim());
            }
        }
        throw new ConditionParseException(literalStr);
    }
}
