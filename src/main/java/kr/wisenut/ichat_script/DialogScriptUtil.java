package kr.wisenut.ichat_script;

import kr.wisenut.ichat_script.data_object.DialogScriptData.DialogScriptRow;

import java.util.List;

public class DialogScriptUtil {

    /**
     * 명령어 데이터 출력
     * 'rowList(명령어 리스트들)'를 출력
     * @param rowList : List<DialogScriptRow>를 출력
     */
    public static void printData(List<DialogScriptRow> rowList) {
        for (DialogScriptRow dialogScriptRow : rowList) {
            String cmd = dialogScriptRow.getCmd().toString();
            String label = dialogScriptRow.getLabel();
            if (cmd.equals("sendMessage")) {
                System.out.println(label + "  " + cmd + "  " + dialogScriptRow.getArguments());
            } else if (cmd.equals("askAndListen")) {
                System.out.println(label + "  " + cmd + "  " + dialogScriptRow.getArguments());
            } else if (cmd.equals("JumpToLabel_if")) {
                System.out.println(label + "  " + cmd + " " + dialogScriptRow.getArguments());
            } else if (cmd.equals("JumpToLabel")) {
                System.out.println(label + "  " + cmd + "  " + dialogScriptRow.getArguments());
            } else if (cmd.equals("JumpToLabel_switch")) {
                System.out.println(label + "  " + cmd + dialogScriptRow.getArguments());
            }
        }
    }

}
