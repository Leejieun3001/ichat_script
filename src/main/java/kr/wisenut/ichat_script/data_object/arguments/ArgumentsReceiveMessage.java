package kr.wisenut.ichat_script.data_object.arguments;

import kr.wisenut.ichat_script.data_object.DialogScriptData.DialogScriptCmdType;
import kr.wisenut.ichat_script.data_object.DialogScriptData.ScriptCmdArguments;

public class ArgumentsReceiveMessage implements ScriptCmdArguments {

    private String messageFromUser;

    public String getMessageFromUser() {
        return messageFromUser;
    }

    public void setMessageFromUser(String messageFromUser) {
        this.messageFromUser = messageFromUser;
    }

    public ArgumentsReceiveMessage(String messageFromUser) {
        this.messageFromUser = messageFromUser;
    }

    @Override
    public DialogScriptCmdType getType() {
        return DialogScriptCmdType.receiveMessage;
    }
}