package kr.wisenut.ichat_script.data_object.arguments;

import kr.wisenut.ichat_script.data_object.DialogScriptData.DialogScriptCmdType;
import kr.wisenut.ichat_script.data_object.DialogScriptData.ScriptCmdArguments;

import java.util.List;

public class ArgumentsJumpToLabel_switch implements ScriptCmdArguments {

    private String variable;
    private List<SwitchCaseValue> labelList;

    @Override
    public String toString() {
        return "ArgumentsJumpToLabel_switch{" +
                "variable='" + variable + '\'' +
                ", labelList=" + labelList +
                '}';
    }

    public ArgumentsJumpToLabel_switch(String variable, List<SwitchCaseValue> labelList) {
        this.variable = variable;
        this.labelList = labelList;
    }

    public String getVariable() {
        return variable;
    }

    public void setVariable(String variable) {
        this.variable = variable;
    }

    public List<SwitchCaseValue> getLabelList() {
        return labelList;
    }

    public void setLabelList(List<SwitchCaseValue> labelList) {
        this.labelList = labelList;
    }

    @Override
    public DialogScriptCmdType getType() {
        return DialogScriptCmdType.JumpToLabel_switch;
    }
}
