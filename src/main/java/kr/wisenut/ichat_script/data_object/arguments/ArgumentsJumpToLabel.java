package kr.wisenut.ichat_script.data_object.arguments;

import kr.wisenut.ichat_script.data_object.DialogScriptData.DialogScriptCmdType;
import kr.wisenut.ichat_script.data_object.DialogScriptData.ScriptCmdArguments;


public class ArgumentsJumpToLabel implements ScriptCmdArguments {
    private String label;

    @Override
    public DialogScriptCmdType getType() {
        return DialogScriptCmdType.JumpToLabel;
    }

    public ArgumentsJumpToLabel(String label) {
        this.label = label;
    }

    public String getLabel() {
        return label;
    }

    @Override
    public String toString() {
        return "ArgumentsJumpToLabel{" +
                "label='" + label + '\'' +
                '}';
    }

    public void setLabel(String label) {
        this.label = label;
    }
}
