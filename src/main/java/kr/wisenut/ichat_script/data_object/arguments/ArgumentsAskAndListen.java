package kr.wisenut.ichat_script.data_object.arguments;


import kr.wisenut.ichat_script.data_object.DialogScriptData.DialogScriptCmdType;
import kr.wisenut.ichat_script.data_object.DialogScriptData.ScriptCmdArguments;

public class ArgumentsAskAndListen implements ScriptCmdArguments {
    private String variable;
    private String dataType;
    private String promptMessage;


    public ArgumentsAskAndListen(String variable, String dataType, String promptMessage) {
        this.variable = variable;
        this.dataType = dataType;
        this.promptMessage = promptMessage;
    }

    public String getVariable() {
        return variable;
    }

    public void setVariable(String variable) {
        this.variable = variable;
    }

    public String getDataType() {
        return dataType;
    }

    public void setDataType(String dataType) {
        this.dataType = dataType;
    }

    public String getPromptMessage() {
        return promptMessage;
    }

    public void setPromptMessage(String promptMessage) {
        this.promptMessage = promptMessage;
    }

    @Override
    public String toString() {
        return "ArgumentsAskAndListen{" +
                "variable='" + variable + '\'' +
                ", dataType='" + dataType + '\'' +
                ", promptMessage='" + promptMessage + '\'' +
                '}';
    }

    @Override
    public DialogScriptCmdType getType() {
        return DialogScriptCmdType.askAndListen;
    }
}
