package kr.wisenut.ichat_script.data_object.arguments;

import kr.wisenut.ichat_script.data_object.DialogScriptData.DialogScriptCmdType;
import kr.wisenut.ichat_script.data_object.DialogScriptData.ScriptCmdArguments;

public class ArgumentsSendMessage implements ScriptCmdArguments {

    private  String messageToUser;

    public String getMessageToUser() {
        return messageToUser;
    }

    public void setMessageToUser(String messageToUser) {
        this.messageToUser = messageToUser;
    }

    public ArgumentsSendMessage(String messageToUser) {
        this.messageToUser = messageToUser;
    }

    @Override
    public DialogScriptCmdType getType() {
        return DialogScriptCmdType.sendMessage;
    }

    @Override
    public String toString() {
        return "ArgumentsSendMessage{" +
                "messageToUser='" + messageToUser + '\'' +
                '}';
    }
}
