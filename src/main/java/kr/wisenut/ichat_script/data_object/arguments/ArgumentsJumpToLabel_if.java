package kr.wisenut.ichat_script.data_object.arguments;


import kr.wisenut.ichat_script.data_object.DialogScriptData.DialogScriptCmdType;
import kr.wisenut.ichat_script.data_object.DialogScriptData.ScriptCmdArguments;

public class ArgumentsJumpToLabel_if implements ScriptCmdArguments {
    private String trueLabel;
    private String falseLabel;
    private String conditional;



    public ArgumentsJumpToLabel_if(String trueLabel, String falseLabel, String conditional) {
        this.trueLabel = trueLabel;
        this.falseLabel = falseLabel;
        this.conditional = conditional;
    }

    public String getTrueLabel() {
        return trueLabel;
    }

    public void setTrueLabel(String trueLabel) {
        this.trueLabel = trueLabel;
    }

    public String getFalseLabel() {
        return falseLabel;
    }

    public void setFalseLabel(String falseLabel) {
        this.falseLabel = falseLabel;
    }

    public String getConditional() {
        return conditional;
    }

    @Override
    public String toString() {
        return "ArgumentsJumpToLabel_if{" +
                "trueLabel='" + trueLabel + '\'' +
                ", falseLabel='" + falseLabel + '\'' +
                ", conditional='" + conditional + '\'' +
                '}';
    }

    public void setConditional(String conditional) {
        this.conditional = conditional;
    }

    @Override
    public DialogScriptCmdType getType() {
        return DialogScriptCmdType.JumpToLabel_if;
    }
}
