package kr.wisenut.ichat_script.data_object.DialogScriptData;

//한 행 씩 저장 - (레이블 label, 함수명 cmd, 인자들  arguments... ,)
public class DialogScriptRow {
    private String label; // 레이블
    private DialogScriptCmdType cmd; // 함수
    private ScriptCmdArguments arguments; // 인자

    public DialogScriptRow(String label, DialogScriptCmdType cmd, ScriptCmdArguments arguments) {
        this.label = label;
        this.cmd = cmd;
        this.arguments = arguments;
    }

    public DialogScriptRow() {

    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public DialogScriptCmdType getCmd() {
        return cmd;
    }

    public void setCmd(DialogScriptCmdType cmd) {
        this.cmd = cmd;
    }

    public ScriptCmdArguments getArguments() {
        return arguments;
    }

    public void setArguments(ScriptCmdArguments arguments) {
        this.arguments = arguments;
    }

    @Override
    public String toString() {
        return "DialogScriptRow{" +
                "label='" + label + '\'' +
                ", cmd=" + cmd +
                ", arguments=" + arguments +
                '}';
    }
}
