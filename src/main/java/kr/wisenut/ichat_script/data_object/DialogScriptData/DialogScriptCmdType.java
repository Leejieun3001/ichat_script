package kr.wisenut.ichat_script.data_object.DialogScriptData;

public enum DialogScriptCmdType {
    sendMessage, askAndListen, askAndListen_choice, JumpToLabel_if, JumpToLabel, JumpToLabel_switch, receiveMessage
}
