package kr.wisenut.ichat_script.data_object.DialogScriptData;

public interface ScriptCmdArguments {
    DialogScriptCmdType getType();
}
