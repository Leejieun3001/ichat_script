package kr.wisenut.ichat_script.data_object.DialogScriptData;

import java.util.List;

public class DialogScript {
    List<DialogScriptRow> rowList;

    public DialogScript() {
    }

    public DialogScript(List<DialogScriptRow> rowList) {
        this.rowList = rowList;
    }

    public List<DialogScriptRow> getRowList() {
        return rowList;
    }

    public void setRowList(List<DialogScriptRow> rowList) {
        this.rowList = rowList;
    }
}
