package kr.wisenut.ichat_script.data_object.parser;

import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormatter;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class ValueConverter {
    public static String delimeter = ",";
    static DateTimeFormatter defaultDTF = org.joda.time.format.DateTimeFormat.forPattern("yyyy-MM-dd HH:mm");
    static DateTimeFormatter defaultDF = org.joda.time.format.DateTimeFormat.forPattern("yyyy-MM-dd");
    static DateTimeFormatter defaultTF = org.joda.time.format.DateTimeFormat.forPattern("HH:mm");

    public static String[] parseStrArray(String value) {
        String[] array = value.split(delimeter);
        return array;
    }

    public static String toString(String[] strArray) {
        StringBuffer strbuf = new StringBuffer();
        for (String s : strArray) {
            strbuf.append(s).append(",");
        }
        if (strbuf.length() > 1) {
            strbuf.deleteCharAt(strbuf.length() - 1);
        }
        return strbuf.toString();
    }

    public static Double parseDouble(String value) {
        return Double.parseDouble(value);
    }

    public static String toString(Double doubleValue) {
        DecimalFormat decimalFormat = new DecimalFormat("####.#####");
        return decimalFormat.format(doubleValue);
    }

    public static Double[] parseDoubleArray(String value) {
        String[] array = value.split(delimeter);
        List<Double> retList = new ArrayList<>();
        for (String s : array) {
            retList.add(Double.parseDouble(s));
        }
        return retList.toArray(new Double[retList.size()]);
    }

    public static String toString(Double[] doubleArray) {
        StringBuffer strbuf = new StringBuffer();
        for (Double v : doubleArray) {
            strbuf.append(toString(v)).append(",");
        }
        if (strbuf.length() > 1) {
            strbuf.deleteCharAt(strbuf.length() - 1);
        }
        return strbuf.toString();
    }

    public static Integer parseInteger(String value) {
        return Integer.parseInt(value);
    }

    public static String toString(Integer value) {
        return value.toString();
    }

    public static Integer[] parseIntegerArray(String value) {
        String[] array = value.split(delimeter);
        List<Integer> retList = new ArrayList<>();
        for (String s : array) {
            retList.add(Integer.parseInt(s));
        }
        return retList.toArray(new Integer[retList.size()]);
    }

    public static String toString(Integer[] intArray) {
        StringBuffer strbuf = new StringBuffer();
        for (Integer v : intArray) {
            strbuf.append(toString(v)).append(",");
        }
        if (strbuf.length() > 1) {
            strbuf.deleteCharAt(strbuf.length() - 1);
        }
        return strbuf.toString();
    }

    public static Boolean parseBoolean(String value) {
        return Boolean.parseBoolean(value);
    }

    public static String toString(Boolean value) {
        return value.toString();
    }


    public static Date parseDateTime(String value) throws IllegalArgumentException {
        DateTime dt = DateTime.parse(value, defaultDTF);
        return dt.toDate();
    }

    public static Date parseDate(String value) throws IllegalArgumentException {
        DateTime dt = DateTime.parse(value, defaultDF);
        return dt.toDate();
    }

    public static Date parseTime(String value) throws IllegalArgumentException {
        DateTime dt = DateTime.parse(value, defaultTF);
        return dt.toDate();
    }

    public static String toDateTimeString(Date date) {
        return defaultDTF.print(new DateTime(date));
    }

    public static String toDateString(Date date) {
        return defaultDF.print(new DateTime(date));
    }

    public static String toTimeString(Date date) {
        return defaultTF.print(new DateTime(date));
    }

//    public static Date parseDate(String value, String datePattern) {
//        DateTimeFormatter dtf = org.joda.time.format.DateTimeFormat.forPattern(datePattern);
//        DateTime dt = DateTime.parse(value, dtf);
//        return dt.toDate();
//    }
//
//    public static String toString(Date date) {
//        return defaultDTF.print(new DateTime(date));
//    }
//
//    public static String toString(Date date, String datePattern) {
//        DateTimeFormatter dtf = org.joda.time.format.DateTimeFormat.forPattern(datePattern);
//        return dtf.print(new DateTime(date));
//    }
}
