package kr.wisenut.ichat_script.data_object.parser;

import java.util.List;

public class Filter {
    protected String key;
    protected String operator;
    protected String value;
    protected String groupOperator;
    protected List<Filter> filterList;

    public Filter() {

    }

    public Filter(OperatorType operatorType) {
        this.operator = operatorType.getValue();
    }

    public Filter(String key, OperatorType operatorType, String value) {
        this.key = key;
        this.operator = operatorType.getValue();
        this.value = value;
    }

    public String getKey() {
        return key;
    }

    public void setKeyValue(String key, String value) {
        this.key = key;
        this.value = value;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public OperatorType getOperator() {
        return OperatorType.fromText(operator);
    }

    public void setOperator(OperatorType operatorType) {
        this.operator = operatorType.getValue();
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public List<Filter> getFilterList() {
        return filterList;
    }

    public String getGroupOperatorStr() {
        return groupOperator;
    }

    @Override
    public String toString() {
        return "Filter{" +
                "key='" + key + '\'' +
                ", operator='" + operator + '\'' +
                ", value='" + value + '\'' +
                ", groupOperator='" + groupOperator + '\'' +
                ", filterList=" + filterList +
                '}';
    }
}
