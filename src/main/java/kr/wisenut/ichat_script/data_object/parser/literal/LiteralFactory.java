package kr.wisenut.ichat_script.data_object.parser.literal;



import kr.wisenut.ichat_script.data_object.parser.LiteralOperator;

import java.util.Date;

public class LiteralFactory {
    public enum TemporalType {
        DateTime, DateOnly, TimeOnly
    }

    public static Literal literal(String argument, LiteralOperator operator, String constant) {
        return new StringLiteral(argument, operator, constant);
    }

    public static Literal literal(Integer argument, LiteralOperator operator, Integer constant) {
        return new IntegerLiteral(argument, operator, constant);
    }

    public static Literal literal(Double argument, LiteralOperator operator, Double constant) {
        return new RealNumberLiteral(argument, operator, constant);
    }

    public static Literal literal(Date argument, LiteralOperator operator, Date constant) {
        return new DateTimeLiteral(argument, operator, constant);
    }

    public static Literal literal(Date argument, LiteralOperator operator, Date constant, TemporalType temporalType) {
        switch (temporalType) {
            case DateOnly:
                return new DateLiteral(argument, operator, constant);
            case TimeOnly:
                return new TimeLiteral(argument, operator, constant);
            default:
                return new DateTimeLiteral(argument, operator, constant);
        }
    }
}

