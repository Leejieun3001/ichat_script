package kr.wisenut.ichat_script.data_object.parser;

import java.util.List;

/**
 * 하나의 조건절을 나타냄
 * 조건절은 여러개의 조건(literal)을 가짐
 * 하나의 조건절에 속하는 모든 조건은 or 로 연결됨
 */
public class ConditionClause {
    private List<ConditionLiteral> literalList;

    public ConditionClause(List<ConditionLiteral> literalList) {
        this.literalList = literalList;
    }

    public List<ConditionLiteral> getLiteralList() {
        return literalList;
    }

    @Override
    public String toString() {
        return "ConditionClause{" +
                "literalList=" + literalList +
                '}';
    }
}
