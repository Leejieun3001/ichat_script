package kr.wisenut.ichat_script.data_object.parser;

import java.util.*;

public class TimedKeyValue implements Comparable<TimedKeyValue> {
    private String key;
    private String valueType;
    private String value;
    private Date time;

    public TimedKeyValue() {

    }

    public TimedKeyValue(TypedKeyValue typedKeyValue) {
        this.key = typedKeyValue.getKey();
        this.valueType = typedKeyValue.getValueType().getValue();
        this.value = typedKeyValue.getValue();
        this.time = new Date();
    }

    public TimedKeyValue(String key, String value) {
        this.key = key;
        this.valueType = ValueType.String.getValue();
        this.value = value;
        this.time = new Date();
    }

    public TimedKeyValue(String key, ValueType valueType, String value) {
        this.key = key;
        this.valueType = valueType.getValue();
        this.value = value;
        this.time = new Date();
    }

    public TimedKeyValue(String key, ValueType valueType, String value, Date time) {
        this.key = key;
        this.valueType = valueType.getValue();
        this.value = value;
        this.time = time;
    }

    public String getKey() {
        return key;
    }

    public String getValue() {
        return value;
    }
    public String getDictValue() throws EntityValueTypeMismatchException {
        if (getValueType() != ValueType.String) {
            throw new EntityValueTypeMismatchException();
        }

        return getValue();
    }

    public Integer getIntegerValue() throws EntityValueTypeMismatchException {
        if (getValueType() != ValueType.Integer) {
            throw new EntityValueTypeMismatchException();
        }
        try {
            return Integer.parseInt(value);
        } catch (NumberFormatException e) {
            throw new EntityValueTypeMismatchException();
        }
    }

    public Double getDoubleValue() throws EntityValueTypeMismatchException {
        if (getValueType() != ValueType.RealNumber) {
            throw new EntityValueTypeMismatchException();
        }
        try {
            return Double.parseDouble(value);
        } catch (NumberFormatException e) {
            throw new EntityValueTypeMismatchException();
        }
    }

    public Date getDateValue() throws EntityValueTypeMismatchException {
        if (getValueType() != ValueType.Date) {
            throw new EntityValueTypeMismatchException();
        }
        try {
            return ValueConverter.parseDate(value);
        } catch (IllegalArgumentException e) {
            throw new EntityValueTypeMismatchException();
        }
    }

    public Date getTimeValue() throws EntityValueTypeMismatchException {
        if (getValueType() != ValueType.Time) {
            throw new EntityValueTypeMismatchException();
        }
        try {
            return ValueConverter.parseTime(value);
        } catch (IllegalArgumentException e) {
            throw new EntityValueTypeMismatchException();
        }
    }

    public Date getDateTimeValue() throws EntityValueTypeMismatchException {
        if (getValueType() != ValueType.DateTime) {
            throw new EntityValueTypeMismatchException();
        }
        try {
            return ValueConverter.parseDateTime(value);
        } catch (IllegalArgumentException e) {
            throw new EntityValueTypeMismatchException();
        }
    }
    public ValueType getValueType() {
        return ValueType.fromText(valueType);
    }

    public Date getTime() {
        return time;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        TimedKeyValue keyValue = (TimedKeyValue) o;
        return Objects.equals(key, keyValue.key) &&
                Objects.equals(valueType, keyValue.valueType) &&
                Objects.equals(value, keyValue.value);
    }

    @Override
    public int hashCode() {
        return Objects.hash(key, valueType, value);
    }

    @Override
    public String toString() {
        return "TimedKeyValue{" +
                "key='" + key + '\'' +
                ", valueType='" + valueType + '\'' +
                ", value='" + value + '\'' +
                ", time=" + time +
                '}';
    }

    @Override
    public int compareTo(TimedKeyValue o) {
        return -time.compareTo(o.time);
    }


    public static void main(String[] args) {
        List<TimedKeyValue> list = new ArrayList<>();
        list.add(new TimedKeyValue("1", "1"));
        try {
            Thread.sleep(2000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        list.add(new TimedKeyValue("2", "2"));

        Collections.sort(list);
        System.out.println(list);
    }
}
