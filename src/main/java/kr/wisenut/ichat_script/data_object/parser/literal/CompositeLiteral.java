package kr.wisenut.ichat_script.data_object.parser.literal;


import kr.wisenut.ichat_script.data_object.parser.DisjunctiveClause;

import java.util.ArrayList;
import java.util.List;

/**
 * 다른 조건을 가지고 있는 Literal
 */
public class CompositeLiteral implements Literal {

    private List<DisjunctiveClause> clauses;

    public CompositeLiteral() {

    }

    public CompositeLiteral(DisjunctiveClause... clauses) {
        this.clauses = new ArrayList();
        for (DisjunctiveClause clause : clauses) {
            this.clauses.add(clause);
        }
    }

    @Override
    public boolean isTrue() {
        for (DisjunctiveClause clause : clauses) {
            if (clause.isFalse()) {
                return false;
            }
        }
        return true;
    }

    public void addClause(DisjunctiveClause clause) {
        if (this.clauses == null) {
            this.clauses = new ArrayList<>();
        }
        this.clauses.add(clause);
    }
}
