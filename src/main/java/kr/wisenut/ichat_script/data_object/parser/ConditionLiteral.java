package kr.wisenut.ichat_script.data_object.parser;

public class ConditionLiteral {
    public static final String SEPARATOR = ",";
    private String argument;
    private LiteralOperator operator;
    private String constant;

    public ConditionLiteral(String argument, LiteralOperator operator, String constant) {
        this.argument = argument;
        this.operator = operator;
        this.constant = constant;
    }

    public String getArgument() {
        return argument;
    }

    public LiteralOperator getOperator() {
        return operator;
    }

    public String getConstant() {
        return constant;
    }

    @Override
    public String toString() {
        return "ConditionLiteral{" +
                "argument='" + argument + '\'' +
                ", operator=" + operator +
                ", constant='" + constant + '\'' +
                '}';
    }
}
