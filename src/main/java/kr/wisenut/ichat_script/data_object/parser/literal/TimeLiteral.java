package kr.wisenut.ichat_script.data_object.parser.literal;

import kr.wisenut.ichat_script.data_object.parser.LiteralOperator;
import org.joda.time.DateTime;
import org.joda.time.DateTimeComparator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Date;

public class TimeLiteral implements Literal {
    private DateTime argument;
    private LiteralOperator operator;
    private DateTime constant;
    private static Logger logger = LoggerFactory.getLogger(TimeLiteral.class);

    public TimeLiteral(Date argument, LiteralOperator operator, Date constant) {
        this.argument = new DateTime(argument.getTime());
        this.operator = operator;
        this.constant = new DateTime(constant.getTime());
    }


    @Override
    public boolean isTrue() {
        int result = DateTimeComparator.getTimeOnlyInstance().compare(new DateTime(argument), new DateTime(constant));
        switch (operator) {
            case Equal:
                return result == 0;
            case NotEqual:
                return result != 0;
            case Greater:
                return result > 0;
            case GreaterEq:
                return result >= 0;
            case Less:
                return result < 0;
            case LessEq:
                return result <= 0;
            default:
                break;
        }
        logger.warn("The operator {} is not acceptable for this literal.", operator);
        return false;
    }


    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("IntegerLiteral{");
        sb.append("argument=").append(argument);
        sb.append(", operator=").append(operator);
        sb.append(", constant=").append(constant);
        sb.append('}');
        return sb.toString();
    }

    public static void main(String[] args) {
        TimeLiteral dateLiteral = new TimeLiteral(new Date(), LiteralOperator.LessEq, new Date());
        logger.info("{}", dateLiteral.isTrue());

    }
}

