package kr.wisenut.ichat_script.data_object.parser;

import kr.wisenut.ichat_script.data_object.parser.literal.Literal;

import static kr.wisenut.ichat_script.data_object.parser.ConjunctiveNormalFormFactory.clause;
import static kr.wisenut.ichat_script.data_object.parser.ConjunctiveNormalFormFactory.cnf;
import static kr.wisenut.ichat_script.data_object.parser.literal.LiteralFactory.literal;


import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class ConditionHandler {
    public ConditionHandler() {

    }

    /**
     * 하나의 조건분기에 속한 조건절들을 받아 모든 조건절이 만족하는지 확인
     *
     * @param conditionClauseList
     * @return
     */


    public boolean isMatchedCondition(List<ConditionClause> conditionClauseList, Map<String, TypedKeyValue> inputMap) throws IOException, EntityValueTypeMismatchException {
        List<DisjunctiveClause> cnfClauseList = new ArrayList<>();
        for (ConditionClause conditionClause : conditionClauseList) {
            List<ConditionLiteral> conditionLiteralList = conditionClause.getLiteralList();
            List<Literal> cnfLiteralList = new ArrayList<>();
            for (ConditionLiteral conditionLiteral : conditionLiteralList) {
                String key = conditionLiteral.getArgument();
                TypedKeyValue input = inputMap.get(key);
                Literal literal = generateLiteralFromMemory(conditionLiteral, input);
                cnfLiteralList.add(literal);
            }
            DisjunctiveClause clause = clause(cnfLiteralList.toArray(new Literal[cnfLiteralList.size()]));
            cnfClauseList.add(clause);
        }

        ConjunctiveNormalForm cnf = cnf(cnfClauseList.toArray(new DisjunctiveClause[cnfClauseList.size()]));
        return cnf.isTrue();
    }


    private Literal generateLiteralFromMemory(ConditionLiteral literal, TypedKeyValue input) throws EntityValueTypeMismatchException {
        LiteralOperator operator = literal.getOperator();
        String value = literal.getConstant();

        switch (input.getValueType()) {
            case String:
                return  literal(value, operator, input.getValue());
            case Integer:
                Integer iConstant = Integer.parseInt(value);
                Integer iArgument = input.getIntegerValue();
                return  literal(iArgument, operator, iConstant);
            case RealNumber:
                Double dConstant = Double.parseDouble(value);
                Double dArgument = input.getDoubleValue();
                return  literal(dArgument, operator, dConstant);
            default:
                throw new EntityValueTypeMismatchException();
        }
    }
}
