package kr.wisenut.ichat_script.data_object.parser;

import java.util.Arrays;


public enum ValueType {
    String("STRING"), RealNumber("REALNUMBER"), Integer("INTEGER"), Date("DATE"), Time("TIME"), DateTime("DATETIME"), Undefined("UNDEFINED");
    private String value;

    ValueType(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }

    public static String[] strValues() {
        ValueType[] arr = ValueType.values();
        String[] strArray = new String[arr.length];
        for (int i = 0; i < arr.length; i++) {
            strArray[i] = arr[i].value;
        }
        return strArray;
    }

    public static ValueType fromText(String text) {
        return Arrays.stream(values())
                .filter(bl -> bl.value.equalsIgnoreCase(text))
                .findFirst()
                .orElse(Undefined);
    }

}
