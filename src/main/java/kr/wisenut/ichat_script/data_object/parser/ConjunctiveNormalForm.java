package kr.wisenut.ichat_script.data_object.parser;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

public class ConjunctiveNormalForm {
    private List<DisjunctiveClause> clauses;

    public ConjunctiveNormalForm() {

    }

    public ConjunctiveNormalForm(DisjunctiveClause... clauses) {
        this.clauses = new ArrayList();
        for (DisjunctiveClause clause : clauses) {
            this.clauses.add(clause);
        }
    }

    public ConjunctiveNormalForm(Collection<DisjunctiveClause> clauses) {
        this.clauses = new ArrayList(clauses);
    }

    public boolean isTrue() {
        for (DisjunctiveClause clause : clauses) {
            if (clause.isFalse()) {
                return false;
            }
        }
        return true;
    }

    public void addClause(DisjunctiveClause clause){
        if (this.clauses == null) {
            this.clauses= new ArrayList<>();
        }
        this.clauses.add(clause);
    }
}
