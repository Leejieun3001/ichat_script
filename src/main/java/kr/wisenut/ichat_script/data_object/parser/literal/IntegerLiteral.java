package kr.wisenut.ichat_script.data_object.parser.literal;

import kr.wisenut.ichat_script.data_object.parser.LiteralOperator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class IntegerLiteral implements Literal {
    private Integer argument;
    private LiteralOperator operator;
    private Integer constant;
    private static Logger logger = LoggerFactory.getLogger(IntegerLiteral.class);

    public IntegerLiteral(Integer argument, LiteralOperator operator, Integer constant) {
        this.argument = argument;
        this.operator = operator;
        this.constant = constant;
    }

    @Override
    public boolean isTrue() {
        switch (operator) {
            case Equal:
                return this.argument == this.constant;
            case NotEqual:
                return !(this.argument == this.constant);
            case Greater:
                return this.argument > this.constant;
            case GreaterEq:
                return this.argument >= this.constant;
            case Less:
                return this.argument < this.constant;
            case LessEq:
                return this.argument <= this.constant;
            default:
                break;
        }
        logger.warn("The operator {} is not acceptable for this literal.", operator);
        return false;
    }


    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("IntegerLiteral{");
        sb.append("argument=").append(argument);
        sb.append(", operator=").append(operator);
        sb.append(", constant=").append(constant);
        sb.append('}');
        return sb.toString();
    }

    public static void main(String[] args) {
        Literal literal = new IntegerLiteral(3, LiteralOperator.GreaterEq, 3);
        System.out.println(literal.isTrue());
    }
}

