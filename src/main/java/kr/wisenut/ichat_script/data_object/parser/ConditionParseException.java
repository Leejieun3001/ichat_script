package kr.wisenut.ichat_script.data_object.parser;

public class ConditionParseException extends Exception {

    public ConditionParseException(){
        super("조건표현식을 변환하는 과정에서 오류가 발생했습니다. 조건표현식을 점검해주세요.");
    }

    public ConditionParseException(String condition){
        super("조건식을 변환하는 과정에서 오류가 발생했습니다. 조건식을 점검해주세요. [ 조건식 - " + condition + " ]");
    }
}
