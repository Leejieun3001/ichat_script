package kr.wisenut.ichat_script.data_object.parser;

public class EntityValueTypeMismatchException extends Exception {
    public EntityValueTypeMismatchException() {}
    public EntityValueTypeMismatchException(String message) { super(message); }

}
