package kr.wisenut.ichat_script.data_object.parser;

import java.util.Arrays;

public enum LiteralOperator {
    Equal("=="), NotEqual("!="), GreaterEq(">="), LessEq("<="), Greater(">"), Less("<"),
    NotContain("!IN"), NotStartWith("!START"), NotEndWith("!END"), NotMatch("!MATCH"),
    Contain("IN"), StartWith("START"), EndWith("END"), Match("MATCH"),
    Undefined("UNDEFINED");

    private String value;

    LiteralOperator(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }

    public static String[] strValues() {
        LiteralOperator[] arr = LiteralOperator.values();
        String[] strArray = new String[arr.length];
        for (int i = 0; i < arr.length; i++) {
            strArray[i] = arr[i].value;
        }
        return strArray;
    }

    public static LiteralOperator fromText(String text) {
        return Arrays.stream(values())
                .filter(bl -> bl.value.equalsIgnoreCase(text))
                .findFirst()
                .orElse(Undefined);
    }
}
