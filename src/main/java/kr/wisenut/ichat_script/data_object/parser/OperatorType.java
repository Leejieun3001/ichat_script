package kr.wisenut.ichat_script.data_object.parser;

import java.util.Arrays;

public enum OperatorType {
    Eq("eq"), Not("not"),In("in"),NotIn("notin"),
    Greater("ge"), Less("le"),GreaterThanEqual("gte"), LessThanEqual("lte"),
    Undefined("UNDEFINED");

    private String value;


    OperatorType(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }

    public static String[] strValues() {
        OperatorType[] arr = OperatorType.values();
        String[] strArray = new String[arr.length];
        for (int i = 0; i < arr.length; i++) {
            strArray[i] = arr[i].value;
        }
        return strArray;
    }

    public static OperatorType fromText(String text) {
        return Arrays.stream(values())
                .filter(bl -> bl.value.equalsIgnoreCase(text))
                .findFirst()
                .orElse(Undefined);
    }
}
