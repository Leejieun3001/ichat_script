package kr.wisenut.ichat_script.data_object.parser.literal;

public interface Literal {
    boolean isTrue();
}
