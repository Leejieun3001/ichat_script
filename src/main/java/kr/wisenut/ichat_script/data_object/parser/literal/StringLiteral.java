package kr.wisenut.ichat_script.data_object.parser.literal;

import kr.wisenut.ichat_script.data_object.parser.LiteralOperator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class StringLiteral implements Literal {
    private String argument;
    private LiteralOperator operator;
    private String constant;
    private static Logger logger = LoggerFactory.getLogger(StringLiteral.class);

    public StringLiteral(String argument, LiteralOperator operator, String constant) {
        this.argument = argument;
        this.operator = operator;
        this.constant = constant;
    }

    @Override
    public boolean isTrue() {
        switch (operator) {
            case Equal:
                return this.argument.equals(this.constant);
            case NotEqual:
                return !this.argument.equals(this.constant);
            case Contain:
                return this.argument.contains(this.constant);
            case Match:
                return this.argument.matches(this.constant);
            case StartWith:
                return this.argument.startsWith(this.constant);
            case EndWith:
                return this.argument.endsWith(this.constant);
            case NotMatch:
                return !this.argument.matches(this.constant);
            case NotStartWith:
                return !this.argument.startsWith(this.constant);
            case NotEndWith:
                return !this.argument.endsWith(this.constant);
            case NotContain:
                return !this.argument.contains(this.constant);
            case Undefined:
            default:
                break;
        }
        logger.warn("The operator {} is not acceptable for this literal.", operator);

        return false;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("StringLiteral{");
        sb.append("argument='").append(argument).append('\'');
        sb.append(", operator=").append(operator);
        sb.append(", constant='").append(constant).append('\'');
        sb.append('}');
        return sb.toString();
    }

    public static void main(String[] args) {
        Literal literal = new StringLiteral("안녕하슈", LiteralOperator.Match, "안녕.*");
        logger.info("{}={}", literal, literal.isTrue());
        literal = new StringLiteral("갑상선암의 증상이 뭐야?", LiteralOperator.NotMatch, "갑상선암의 증상이 뭐야?");
        logger.info("{}={}", literal, literal.isTrue());
    }
}
