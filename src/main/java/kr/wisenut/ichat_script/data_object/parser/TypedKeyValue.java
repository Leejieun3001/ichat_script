package kr.wisenut.ichat_script.data_object.parser;

import java.util.Date;
import java.util.Objects;

/**
 * Key와 Value로 이루어짐.
 * Key가 같으면 equal에서는 같다고 처리함. (map처럼 동작시키기 위함)
 */
public class TypedKeyValue {
    private String key;
    private String valueType;
    private String value;

    public TypedKeyValue() {

    }

    public TypedKeyValue(String key, String value) {
        this.key = key;
        this.valueType = ValueType.String.getValue();
        this.value = value;
    }

    public TypedKeyValue(String key, ValueType valueType, String value) {
        this.key = key;
        this.valueType = valueType.getValue();
        this.value = value;
    }
    public TypedKeyValue(String key, String valueType, String value) {
        this.key = key;
        this.valueType = valueType;
        this.value = value;
    }

    public TypedKeyValue(String key, Double value) {
        this.key = key;
        this.valueType = ValueType.RealNumber.getValue();
        this.value = value.toString();
    }

    public TypedKeyValue(String key, Integer value) {
        this.key = key;
        this.valueType = ValueType.Integer.getValue();
        this.value = value.toString();
    }


    public boolean isEmpty() {
        return key == null || value == null;
    }

    public String getKey() {
        return key;
    }

    public String getValue() {
        return value;
    }

    public String getDictValue() throws EntityValueTypeMismatchException {
        if (getValueType() != ValueType.String) {
            throw new EntityValueTypeMismatchException();
        }

        return getValue();
    }

    public Integer getIntegerValue() throws EntityValueTypeMismatchException {
        if (getValueType() != ValueType.Integer) {
            throw new EntityValueTypeMismatchException();
        }
        try {
            return Integer.parseInt(value);
        } catch (NumberFormatException e) {
            throw new EntityValueTypeMismatchException();
        }
    }

    public Double getDoubleValue() throws EntityValueTypeMismatchException {
        if (getValueType() != ValueType.RealNumber) {
            throw new EntityValueTypeMismatchException();
        }
        try {
            return Double.parseDouble(value);
        } catch (NumberFormatException e) {
            throw new EntityValueTypeMismatchException();
        }
    }

    public Date getDateValue() throws EntityValueTypeMismatchException {
        if (getValueType() != ValueType.Date) {
            throw new EntityValueTypeMismatchException();
        }
        try {
            return ValueConverter.parseDate(value);
        } catch (IllegalArgumentException e) {
            throw new EntityValueTypeMismatchException();
        }
    }

    public Date getTimeValue() throws EntityValueTypeMismatchException {
        if (getValueType() != ValueType.Time) {
            throw new EntityValueTypeMismatchException();
        }
        try {
            return ValueConverter.parseTime(value);
        } catch (IllegalArgumentException e) {
            throw new EntityValueTypeMismatchException();
        }
    }

    public Date getDateTimeValue() throws EntityValueTypeMismatchException {
        if (getValueType() != ValueType.DateTime) {
            throw new EntityValueTypeMismatchException();
        }
        try {
            return ValueConverter.parseDateTime(value);
        } catch (IllegalArgumentException e) {
            throw new EntityValueTypeMismatchException();
        }
    }

//    public Double getPercentValue() throws EntityValueTypeMismatchException{
//        if(getValueType() != EntityValueType.Percent){
//            throw new EntityValueTypeMismatchException();
//        }
//        try{
//            value = value.replaceAll("%", "");
//            return Double.parseDouble(value);
//        }catch(Exception e){
//            throw new EntityValueTypeMismatchException();
//        }
//    }

    public ValueType getValueType() {
        if(valueType==null)
            return null;
        return ValueType.fromText(valueType);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        TypedKeyValue keyValue = (TypedKeyValue) o;
        return Objects.equals(key, keyValue.key) &&
                Objects.equals(valueType, keyValue.valueType) &&
                Objects.equals(value, keyValue.value);
    }

    @Override
    public int hashCode() {
        return Objects.hash(key, valueType, value);
    }

    @Override
    public String toString() {
        return "KeyValue{" +
                "key='" + key + '\'' +
                ", valueType='" + valueType + '\'' +
                ", value='" + value + '\'' +
                '}';
    }
}
