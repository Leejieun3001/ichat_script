package kr.wisenut.ichat_script.data_object.parser;

import kr.wisenut.ichat_script.data_object.parser.literal.Literal;

public class ConjunctiveNormalFormFactory {
    public static ConjunctiveNormalForm cnf(DisjunctiveClause... clauses) {
        return new ConjunctiveNormalForm(clauses);
    }

    public static DisjunctiveClause clause(Literal... literals) {
        return new DisjunctiveClause(literals);
    }
}
