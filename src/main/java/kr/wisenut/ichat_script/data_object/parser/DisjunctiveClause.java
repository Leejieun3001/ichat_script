package kr.wisenut.ichat_script.data_object.parser;

import kr.wisenut.ichat_script.data_object.parser.literal.Literal;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

public class DisjunctiveClause {
    List<Literal> literals;

    public DisjunctiveClause() {

    }

    public DisjunctiveClause(Literal... literals) {
        this.literals = new ArrayList();
        for (Literal literal : literals) {
            this.literals.add(literal);
        }
    }

    public DisjunctiveClause(Collection<Literal> literals) {
        this.literals = new ArrayList(literals);
    }

    public boolean isTrue() {
        for (Literal literal : literals) {
            if (literal.isTrue()) {
                return true;
            }
        }
        return false;
    }

    public boolean isFalse() {
        return !isTrue();
    }

    public List<Literal> getLiterals() {
        return literals;
    }

    public void setLiterals(List<Literal> literals) {
        this.literals = literals;
    }

    public void addLiteral(Literal literal) {
        if (this.literals == null) {
            this.literals = new ArrayList<>();
        }
        this.literals.add(literal);
    }
}
