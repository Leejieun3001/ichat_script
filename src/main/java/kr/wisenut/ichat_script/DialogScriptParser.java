package kr.wisenut.ichat_script;

import kr.wisenut.ichat_script.data_object.DialogScriptData.DialogScript;

import java.io.IOException;

public interface DialogScriptParser {
    DialogScript loadCSV(String csvFileName) throws IOException;
}
